package fr.ep.iziglooms.serviceods.toiture.model.mappers;

import fr.ep.iziglooms.serviceods.champmodule.model.dto.ChampModuleLecture;
import fr.ep.iziglooms.serviceods.champmodule.model.entities.ChampModuleEntity;
import fr.ep.iziglooms.serviceods.champmodule.model.mappers.ChampModuleMapper;
import fr.ep.iziglooms.serviceods.common.model.dto.Polygon;
import fr.ep.iziglooms.serviceods.obstacle.model.dto.ObstacleLecture;
import fr.ep.iziglooms.serviceods.obstacle.model.entities.ObstacleEntity;
import fr.ep.iziglooms.serviceods.obstacle.model.mappers.ObstacleMapper;
import fr.ep.iziglooms.serviceods.toiture.model.dto.Toiture;
import fr.ep.iziglooms.serviceods.toiture.model.dto.ToitureLecture;
import fr.ep.iziglooms.serviceods.toiture.model.dto.ToitureLectureBE;
import fr.ep.iziglooms.serviceods.toiture.model.entities.ToitureEntity;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.geo.GeoJsonPolygon;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ToitureMapper {
    ToitureMapper INSTANCE = Mappers.getMapper(ToitureMapper.class);

    @Mappings({
            @Mapping(target="polygone", source="toitureEntity", qualifiedByName = "coordinatesToDto"),
            @Mapping(target="obstacles", source="toitureEntity", qualifiedByName = "obstaclesToDto"),
            @Mapping(target="champModules", source="toitureEntity", qualifiedByName = "champModulesToDto")
    })
    ToitureLecture toDto (ToitureEntity toitureEntity);

    @Mappings({
            @Mapping(target="polygone", ignore = true),
            @Mapping(target="obstacles", source="toitureEntity", qualifiedByName = "obstaclesToDtoBE"),
            @Mapping(target="champModules", source="toitureEntity", qualifiedByName = "champModulesToDtoBE")
    })
    ToitureLectureBE toDtoBE (ToitureEntity toitureEntity);

    @Mappings({
            @Mapping(target="polygone", source="toiture", qualifiedByName = "coordinatesToEntity"),
            @Mapping(target="obstacles", source="toiture", qualifiedByName = "obstaclesToEntity"),
            @Mapping(target="champModules", source="toiture", qualifiedByName = "champModulesToEntity")
    })
    ToitureEntity toEntity (Toiture toiture);


    @Named("coordinatesToDto")
    default Polygon geoJsonPolygonToPolygone(ToitureEntity toitureEntity){
        if(toitureEntity.getPolygone() == null || toitureEntity.getPolygone().getCoordinates().isEmpty()){
            return null;
        }
        Polygon polygone = new Polygon();
        List<List<List<Double>>> collect = toitureEntity.getPolygone().getCoordinates().stream()
                .map(geoJsonLineString -> geoJsonLineString.getCoordinates().stream()
                        .map(point -> {
                            List<Double> coord = new ArrayList<>();
                            coord.add(point.getX());
                            coord.add(point.getY());
                            return coord;
                        })
                        .collect(Collectors.toList()))
                .collect(Collectors.toList());
        polygone.setType(toitureEntity.getPolygone().getType());
        polygone.setCoordinates(collect);
        return polygone;
    }

    @Named("coordinatesToEntity")
    default GeoJsonPolygon coordinatesToGeoJsonPolygon(Toiture toiture) {
        if(toiture.getPolygone() == null || toiture.getPolygone().getCoordinates().isEmpty()){
            return null;
        }
        List<Point> points = toiture.getPolygone().getCoordinates().get(0).stream()
                .map(doubles -> new Point(doubles.get(0), doubles.get(1)))
                .collect(Collectors.toList());
        return new GeoJsonPolygon(points);
    }

    @Named("obstaclesToDto")
    default List<ObstacleLecture> obstaclesToDto(ToitureEntity toitureEntity){
        if(toitureEntity.getObstacles() == null || toitureEntity.getObstacles().isEmpty() ){
            return Collections.emptyList();
        }
        return toitureEntity.getObstacles().stream()
                .map(ObstacleEntity::toDto)
                .collect(Collectors.toList());

    }

    @Named("obstaclesToDtoBE")
    default List<ObstacleLecture> obstaclesToDtoBE(ToitureEntity toitureEntity){
        if(toitureEntity.getObstacles() == null || toitureEntity.getObstacles().isEmpty() ){
            return Collections.emptyList();
        }
        return toitureEntity.getObstacles().stream()
                .map(ObstacleEntity::toDtoBE)
                .collect(Collectors.toList());

    }

    @Named("obstaclesToEntity")
    default List<ObstacleEntity> obstaclesToEntity(Toiture toiture){
        if(toiture.getObstacles() == null || toiture.getObstacles().isEmpty() ){
            return Collections.emptyList();
        }
        return toiture.getObstacles().stream()
                .map(ObstacleMapper.INSTANCE::toEntity)
                .collect(Collectors.toList());
    }

    @Named("champModulesToDto")
    default List<ChampModuleLecture> champModulesToDto(ToitureEntity toitureEntity){
        if(toitureEntity.getChampModules() == null || toitureEntity.getChampModules().isEmpty() ){
            return Collections.emptyList();
        }
        return toitureEntity.getChampModules().stream()
                .map(ChampModuleEntity::toDto)
                .collect(Collectors.toList());

    }

    @Named("champModulesToDtoBE")
    default List<ChampModuleLecture> champModulesToDtoBE(ToitureEntity toitureEntity){
        if(toitureEntity.getChampModules() == null || toitureEntity.getChampModules().isEmpty() ){
            return Collections.emptyList();
        }
        return toitureEntity.getChampModules().stream()
                .map(ChampModuleEntity::toDtoBE)
                .collect(Collectors.toList());

    }

    @Named("champModulesToEntity")
    default List<ChampModuleEntity> champModulesToEntity(Toiture toiture){
        if(toiture.getChampModules() == null || toiture.getChampModules().isEmpty() ){
            return Collections.emptyList();
        }
        return toiture.getChampModules().stream()
                .map(ChampModuleMapper.INSTANCE::toEntity)
                .collect(Collectors.toList());
    }
}
