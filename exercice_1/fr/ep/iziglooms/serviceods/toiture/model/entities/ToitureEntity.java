package fr.ep.iziglooms.serviceods.toiture.model.entities;

import fr.ep.iziglooms.serviceods.champmodule.model.entities.ChampModuleEntity;
import fr.ep.iziglooms.serviceods.obstacle.model.entities.ObstacleEntity;
import fr.ep.iziglooms.serviceods.quantitatif.model.TypeFixation;
import fr.ep.iziglooms.serviceods.toiture.model.dto.ToitureLecture;
import fr.ep.iziglooms.serviceods.toiture.model.dto.ToitureLectureBE;
import fr.ep.iziglooms.serviceods.toiture.model.mappers.ToitureMapper;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.geo.GeoJsonPolygon;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

@Getter
@Setter
@Document(collection="Toitures")
public class ToitureEntity {

    @Id
    private String id;
    private String idProjet;
    private LocalDateTime dateCreation;
    private LocalDateTime dateModification;
    private HashMap<String, Object> caracteristiques;
    private List<ObstacleEntity> obstacles;
    private HashMap<String, Object> module;
    @DBRef
    private List<ChampModuleEntity> champModules;
    private GeoJsonPolygon polygone;
    private FixationEntity fixation;
    private Double azimuth;

    @Getter
    @Setter
    public static class FixationEntity {
        private TypeFixation cle;
        private String libelle;
        private Double ecartementRangee;
        private Double ecartementColonne;
        private Double pressionMaxVent;
        private Double pressionMaxNeige;
    }

    public ToitureLecture toDto(){
        return ToitureMapper.INSTANCE.toDto(this);
    }

    public ToitureLectureBE toDtoBE(){
        return ToitureMapper.INSTANCE.toDtoBE(this);
    }
}
