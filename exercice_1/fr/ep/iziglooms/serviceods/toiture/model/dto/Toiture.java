package fr.ep.iziglooms.serviceods.toiture.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import fr.ep.iziglooms.serviceods.champmodule.model.dto.ChampModuleLecture;
import fr.ep.iziglooms.serviceods.common.model.dto.Polygon;
import fr.ep.iziglooms.serviceods.obstacle.model.dto.ObstacleLecture;
import fr.ep.iziglooms.serviceods.quantitatif.model.TypeFixation;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Toiture {
    private HashMap<String, Object> caracteristiques;
    private List<ObstacleLecture> obstacles;
    private HashMap<String, Object> module;
    private List<ChampModuleLecture> champModules;
    private Polygon polygone;
    private Fixation fixation;
    private Double azimuth;

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Fixation {
        private TypeFixation cle;
        private String libelle;
        private Double ecartementRangee;
        private Double ecartementColonne;
        private Double pressionMaxVent;
        private Double pressionMaxNeige;
    }

    public <T> T getCaracteristique(String name, Class<T> type) {
        Object value = getCaracteristiques().get(name);
        if (value == null) {
            return null;
        }

        return type.cast(value);
    }

    public <T> T getModule(String name, Class<T> type) {
        Object value = getModule().get(name);
        if (value == null) {
            return null;
        }

        return type.cast(value);
    }
}
