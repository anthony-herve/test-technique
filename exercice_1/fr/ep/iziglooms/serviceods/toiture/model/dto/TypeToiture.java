package fr.ep.iziglooms.serviceods.toiture.model.dto;

public enum TypeToiture {
    Terrasse,
    Monopente,
    DeuxPans;
}
