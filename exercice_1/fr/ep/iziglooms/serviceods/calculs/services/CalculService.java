package fr.ep.iziglooms.serviceods.calculs.services;

import com.google.common.collect.Lists;
import com.jillesvangurp.geo.GeoGeometry;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Polygon;
import fr.ep.iziglooms.serviceods.OdsErrorMessages;
import fr.ep.iziglooms.serviceods.calculs.model.CalculModuleInput;
import fr.ep.iziglooms.serviceods.calculs.model.CalculModuleOutput;
import fr.ep.iziglooms.serviceods.calculs.model.CardinalPoint;
import fr.ep.iziglooms.serviceods.calculs.model.MinimumOrientedRectangle;
import fr.ep.iziglooms.serviceods.toiture.model.dto.ToitureModification;
import fr.ep.iziglooms.serviceods.toiture.model.dto.TypeToiture;
import fr.ep.iziglooms.serviceods.toiture.model.entities.ToitureEntity;
import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.CRS;
import org.geotools.referencing.GeodeticCalculator;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.opengis.geometry.MismatchedDimensionException;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;
import org.opengis.referencing.operation.TransformException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Point;
import org.springframework.stereotype.Service;

import java.awt.geom.Point2D;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;


@Service
public class CalculService {

    private static final Logger logger = LoggerFactory.getLogger(CalculService.class);

    private static final String EPSG_4326 = "EPSG:4326";
    private static final String EPSG_3857 = "EPSG:3857";
    private static final String INCLINAISON_TOIT = "inclinaisonToit";
    private static final String TYPE = "type";

    private static final double NORTH_EAST = 45;
    private static final double SOUTH_EAST = 135;
    private static final double SOUTH_WEST = 225;
    private static final double NORTH_WEST = 315;

    private static Point pointOrigine;

    @Autowired
    public CalculService() {
        //
    }

    /**
     * Permet de retourner une bounding box avec le bon azimuth
     * @param calculModuleInput
     * @return
     */
    private MinimumOrientedRectangle getMinimumOrientedRectangleWithAzimuth (CalculModuleInput calculModuleInput, CardinalPoint cardinalPoint){
        try {
            List<Point> points =  calculModuleInput.getChampModule().getCoordinates().get(0).stream()
                    .map(doubles -> new Point(doubles.get(0), doubles.get(1)))
                    .collect(Collectors.toList());

            CoordinateReferenceSystem epsg4326 = CRS.decode(EPSG_4326,true);

            List<Point> points1 = transformPolygonToEPSG4326InEPSG3857Coord(points);
            List<Point[]> allBoundingRectangles = RotatingCalipers.getAllBoundingRectangles(points1);
            TreeMap<Double,List<Point>> map = new TreeMap<>();
            for(Point[] pt : allBoundingRectangles){
                List<Point> points2 = Arrays.asList(pt);
                List<Point> points3 = transformPolygonToEPSG3857InEPSG4326Coord(points2);
                List<Point> minimumBoundingRectangleOrderListByLongitudeInc = orderListByLongitudeInc(points3);
                Point startingPoint;
                Point destinationPoint;

                if(CardinalPoint.NORTH.equals(cardinalPoint) || CardinalPoint.SOUTH.equals(cardinalPoint)){
                    startingPoint = getUpperLeftPoint(minimumBoundingRectangleOrderListByLongitudeInc);
                    destinationPoint = getLowerLeftPoint(minimumBoundingRectangleOrderListByLongitudeInc);
                }else{
                    startingPoint = getUpperLeftPoint(minimumBoundingRectangleOrderListByLongitudeInc);
                    destinationPoint = getUpperRightPoint(minimumBoundingRectangleOrderListByLongitudeInc);
                }

                GeodeticCalculator calcLongueur = new GeodeticCalculator(epsg4326);
                calcLongueur.setStartingGeographicPoint(new Point2D.Double(startingPoint.getX(),startingPoint.getY()));
                calcLongueur.setDestinationGeographicPoint(new Point2D.Double(destinationPoint.getX(),destinationPoint.getY()));
                double azimuth = calcLongueur.getAzimuth();
                if (azimuth < 0) {
                    azimuth += 360;
                }
                azimuth = roundTwoDecimal(azimuth);
                map.put(azimuth,minimumBoundingRectangleOrderListByLongitudeInc);
            }

            if(map.containsKey(180.) && map.size()>1) {
                logger.debug("Remove BBOX oriented to NORTH or/and SOUTH°");
                map.remove(180.);
            }

            return determineBestMinimumOrientedRectangle(map,calculModuleInput.getAzimuth());

        } catch (TransformException | FactoryException e) {
            throw new OdsErrorMessages(OdsErrorMessages.MBBOX_ERREUR, e.getMessage());
        }
    }

    private MinimumOrientedRectangle determineBestMinimumOrientedRectangle(TreeMap<Double,List<Point>> map, Double azimuth){
        Map.Entry<Double,List<Point>> low = map.floorEntry(azimuth);
        Map.Entry<Double,List<Point>> high = map.ceilingEntry(azimuth);
        Double bestAzimuth = 0.;
        List<Point> bestList = null;
        if (low != null && high != null) {
            if(Math.abs(azimuth - low.getKey()) < Math.abs(azimuth - high.getKey())){
                bestAzimuth = low.getKey();
                bestList = low.getValue();
            }else{
                bestAzimuth = high.getKey();
                bestList = high.getValue();
            }
        } else if (low != null || high != null) {
            if(low != null){
                bestAzimuth = low.getKey();
                bestList = low.getValue();
            }else{
                bestAzimuth = high.getKey();
                bestList = high.getValue();
            }
        }
        logger.debug("best azimuth is {}",bestAzimuth);
        return new MinimumOrientedRectangle(bestList,bestAzimuth.longValue());
    }


    private CardinalPoint determineBestCardinalPoint(Double azimuth){
        if(azimuth == null){
            throw new OdsErrorMessages(OdsErrorMessages.CHAMPS_MANQUANT,"azimuth");
        }

        logger.debug("azimuth = {}",azimuth);
        if(azimuth >= NORTH_EAST && azimuth < SOUTH_EAST) {
            logger.debug("EAST");
            return CardinalPoint.EAST;
        }

        if(azimuth >= SOUTH_EAST && azimuth <= SOUTH_WEST){
            logger.debug("SOUTH");
            return CardinalPoint.SOUTH;
        }

        if(azimuth > SOUTH_WEST && azimuth <= NORTH_WEST){
            logger.debug("WEST");
            return CardinalPoint.WEST;
        }

        if(azimuth > NORTH_WEST || azimuth < NORTH_EAST){
            logger.debug("NORTH");
            return CardinalPoint.NORTH;
        }
        throw new OdsErrorMessages(OdsErrorMessages.POINT_CARDINAL_INEXISTANT,String.valueOf(azimuth));
    }

    public CalculModuleOutput calculerModulesGeotools(CalculModuleInput calculModuleInput){
        CalculModuleOutput calculModuleOutput = new CalculModuleOutput();
//        PrintWriter printWriter = null;
        List<fr.ep.iziglooms.serviceods.common.model.dto.Polygon> modules = new ArrayList<>();
        try {
            LocalDateTime dateDebut = LocalDateTime.now();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:sss.SSS", Locale.FRANCE);
            Date debut = format.parse(dateDebut.toString());
//            FileWriter fileWriter = new FileWriter("C:\\Izigloo\\workspace_ods\\test2.json");
//            printWriter = new PrintWriter(fileWriter);

            CardinalPoint cardinalPoint = determineBestCardinalPoint(calculModuleInput.getAzimuth());
            MinimumOrientedRectangle minimumOrientedRectangleWithAzimuth = getMinimumOrientedRectangleWithAzimuth(calculModuleInput, cardinalPoint);
            List<Point> minimumBoundingRectangleOrderListByLongitudeInc = orderListByLongitudeInc(minimumOrientedRectangleWithAzimuth.getPoints());

            //CALCUL de la BBOX
            //             longueur
            //          A-----------D
            //          |           |
            // largeur  |           |
            //          |           |
            //          B-----------C
            // A = upperLeftPoint
            // B = lowerLeftPoint
            // C = lowerRightPoint
            // D = upperRightPoint
            Point upperLeftPoint = getUpperLeftPoint(minimumBoundingRectangleOrderListByLongitudeInc);
            Point upperRightPoint = getUpperRightPoint(minimumBoundingRectangleOrderListByLongitudeInc);
            Point lowerLeftPoint = getLowerLeftPoint(minimumBoundingRectangleOrderListByLongitudeInc);
            Point lowerRightPoint = getLowerRightPoint(minimumBoundingRectangleOrderListByLongitudeInc);

//            printWriter.println("{");
//            printWriter.println("\"type\": \"Feature\",");
//            printWriter.println("\"properties\": {},");
//            printWriter.println("\"geometry\": { \"type\": \"Polygon\",\"coordinates\": [[");
//            printWriter.println("[" + upperLeftPoint.getX() +","+ upperLeftPoint.getY() + "],");
//            printWriter.println("[" + upperRightPoint.getX() +","+ upperRightPoint.getY() + "],");
//            printWriter.println("[" + lowerRightPoint.getX() +","+ lowerRightPoint.getY() + "],");
//            printWriter.println("[" + lowerLeftPoint.getX() +","+ lowerLeftPoint.getY() + "],");
//            printWriter.println("[" + upperLeftPoint.getX() +","+ upperLeftPoint.getY() + "]");
//            printWriter.println("]]}},");

            // A <-> B
            double orthodromicDistanceLargeurRef = calculateDistance(upperLeftPoint,lowerLeftPoint);

            // A <-> D
            double orthodromicDistanceLongueurRef = calculateDistance(upperLeftPoint,upperRightPoint);

            //Angle entre A et B
            double azimuthAB =calculateAzimuth(upperLeftPoint,lowerLeftPoint);

            // Angle entre A et D
            double azimuthAD = calculateAzimuth(upperLeftPoint,upperRightPoint);

            double largeur = 0;
            double longueur = 0;
            double largeurModule = 0;
            double longueurModule = 0;

            switch (calculModuleInput.getTypeFixation()){
                case RoofSolarPVCInclineDoubleShed:
                case RoofSolarBitumeInclineDoubleShed:
                    if(calculModuleInput.getLargeurModule()<=0.){
                        throw new OdsErrorMessages(OdsErrorMessages.CALCUL_MODULE_ERREUR,"Pas de largeur pour les modules");
                    }else{
                        largeurModule = calculModuleInput.getLongueurModule()/1000 ;
                    }
                    if(calculModuleInput.getLongueurModule()<=0.){
                        throw new OdsErrorMessages(OdsErrorMessages.CALCUL_MODULE_ERREUR,"Pas de longueur pour les modules");
                    }else{
                        longueurModule = calculModuleInput.getLargeurModule()/1000;
                    }
                    break;
                default:
                    if(calculModuleInput.getLargeurModule()<=0.){
                        throw new OdsErrorMessages(OdsErrorMessages.CALCUL_MODULE_ERREUR,"Pas de largeur pour les modules");
                    }else{
                        largeurModule = calculModuleInput.getLargeurModule()/1000 ;
                    }
                    if(calculModuleInput.getLongueurModule()<=0.){
                        throw new OdsErrorMessages(OdsErrorMessages.CALCUL_MODULE_ERREUR,"Pas de longueur pour les modules");
                    }else{
                        longueurModule = calculModuleInput.getLongueurModule()/1000;
                    }
                    break;
            }

            double espacementRangees = 0;
            double espacementColonnes = 0;
            if(calculModuleInput.getEspacementRangees()>0){
                espacementRangees = calculModuleInput.getEspacementRangees()/1000;
            }
            if(calculModuleInput.getEspacementColonnes()>0){
                espacementColonnes = calculModuleInput.getEspacementColonnes()/1000;
            }

            largeurModule = calculateProjectedWidth(largeurModule, calculModuleInput.getTypeToiture(), calculModuleInput.getInclinaisonToit());
            espacementRangees = calculateProjectedWidth(espacementRangees, calculModuleInput.getTypeToiture(), calculModuleInput.getInclinaisonToit());

            if(CardinalPoint.EAST.equals(cardinalPoint) || CardinalPoint.WEST.equals(cardinalPoint)){
                double tmp = longueurModule;
                longueurModule = largeurModule;
                largeurModule = tmp;

                tmp = espacementRangees;
                espacementRangees = espacementColonnes;
                espacementColonnes = tmp;
            }

            int nbColonnes = 0;
            int nbRangees = 0;
            int nbModules = 0;
            Point pointDepart = null;
            while (largeur < orthodromicDistanceLargeurRef) {
                boolean flagRangee = false;
                if (largeur == 0) {
                    pointDepart = rotatePointGeoTool(upperLeftPoint,azimuthAB,0.001);
                }else{
                    pointDepart = rotatePointGeoTool(pointDepart,azimuthAB,largeurModule + espacementRangees);
                }
                int tempNbColonnes = 0;
                while (longueur < orthodromicDistanceLongueurRef) {
                    fr.ep.iziglooms.serviceods.common.model.dto.Polygon generateModule;
                    if (longueur == 0) {
                        pointOrigine = pointDepart;
                        generateModule = generateModuleGeoTool(pointOrigine, longueurModule, largeurModule, azimuthAD, 0);
                    }else{
                        generateModule = generateModuleGeoTool(pointOrigine, longueurModule, largeurModule, azimuthAD, espacementColonnes);
                    }
                    double[][] generatePolygone = generatePolygone(generateModule);

                    //Pour tester si les modules sont dans le champ de module
                    double[][] generatePolygoneChampModule = generatePolygone(calculModuleInput.getChampModule());
                    boolean contains = GeoGeometry.contains(generatePolygoneChampModule, generatePolygone);

                    boolean moduleInObstacle = polygoneOverlapObstacle(generatePolygone, calculModuleInput.getObstacles());

                    if(contains && !moduleInObstacle) {
                        flagRangee = true;
//                        int i = 0;
//                        Point pointRef = null;
//                        printWriter.println("{");
//                        printWriter.println("\"type\": \"Feature\",");
//                        printWriter.println("\"properties\": {");
//                        printWriter.println("\"stroke\": \"#a0200a\",");
//                        printWriter.println("\"stroke-width\": 2,");
//                        printWriter.println("\"stroke-opacity\": 1,");
//                        printWriter.println("\"fill\": \"#ff0000\",");
//                        printWriter.println("\"fill-opacity\": 0.5");
//                        printWriter.println("},");
//                        printWriter.println("\"geometry\": { \"type\": \"Polygon\",\"coordinates\": [[");
//                        for (Point point : generateListPoints(generateModule.getCoordinates())) {
//                            if (i == 0) {
//                                pointRef = new Point(point.getX(), point.getY());
//                            }
//                            printWriter.println("[" + point.getX() + "," + point.getY() + "],");
//                            i++;
//                        }
//                        if (pointRef != null) {
//                            printWriter.println("[" + pointRef.getX() + "," + pointRef.getY() + "]");
//                        }
//                        printWriter.println("]]}},");
                        nbModules++;
                        tempNbColonnes++;
                        modules.add(generateModule);
                    }
                    longueur += longueurModule + espacementColonnes;
                }
                if(tempNbColonnes > nbColonnes){
                    nbColonnes = tempNbColonnes;
                }
                if(flagRangee){
                    nbRangees++;
                }
                longueur = 0;
                largeur += largeurModule + espacementRangees;
            }
            LocalDateTime dateFin = LocalDateTime.now();
            Date fin = format.parse(dateFin.toString());
            long diff = (fin.getTime() - debut.getTime());
            logger.debug("nbColonnes = {}, nbRangees = {}, nbModules = {} avec un temps de traitement de {} ms", nbColonnes, nbRangees, nbModules,diff);
            calculModuleOutput.setModules(modules);
            calculModuleOutput.setNombreColonnes(nbColonnes);
            calculModuleOutput.setNombreRangees(nbRangees);
            calculModuleOutput.setNombreModules(nbModules);
        } catch (/*IOException |*/ ParseException e) {
            throw new OdsErrorMessages(OdsErrorMessages.CALCUL_MODULE_ERREUR,e.getMessage());
        } /*finally {
            if(printWriter!=null){
                printWriter.close();
            }
        }*/
        return calculModuleOutput;
    }

    private double calculateProjectedWidth(double largeurModule, TypeToiture typeToiture, double inclinaisonToit) {
        if(TypeToiture.Monopente.equals(typeToiture) || TypeToiture.DeuxPans.equals(typeToiture)){
            return roundTwoDecimal(largeurModule * Math.cos(Math.atan(inclinaisonToit/100)));
        }else{
            return largeurModule;
        }
    }

    private double calculateDistance(Point originPoint, Point destinationPoint) {
        return generateGeodeticCalculator(originPoint, destinationPoint).getOrthodromicDistance();
    }

    private double calculateAzimuth(Point originPoint, Point destinationPoint) {
        double azimuth = generateGeodeticCalculator(originPoint, destinationPoint).getAzimuth();
        if (azimuth < 0) {
            azimuth += 360;
        }
        return azimuth;
    }

    private GeodeticCalculator generateGeodeticCalculator(Point originPoint, Point destinationPoint){
        try {
            CoordinateReferenceSystem epsg4326 = CRS.decode(EPSG_4326,true);
            GeodeticCalculator geodeticCalculator = new GeodeticCalculator(epsg4326);
            geodeticCalculator.setStartingGeographicPoint(new Point2D.Double(originPoint.getX(),originPoint.getY()));
            geodeticCalculator.setDestinationGeographicPoint(new Point2D.Double(destinationPoint.getX(),destinationPoint.getY()));
            return geodeticCalculator;
        } catch (FactoryException e) {
            throw new OdsErrorMessages(OdsErrorMessages.CALCUL_MODULE_ERREUR, "Erreur lors de la génération d'un GeodeticCalculator");
        }
    }

    //méthode pour la distance au bord
    private Geometry polygonBuffer(List<Point> points, double distance) {
        Geometry polygon = createPolygon(points);
        // extract the geometry
        Geometry ppolygon = polygon;
        MathTransform toTransform = null;
        MathTransform fromTransform = null;

        com.vividsolutions.jts.geom.Point c = polygon.getCentroid();
        double x = c.getCoordinate().x;
        double y = c.getCoordinate().y;

        String code = "AUTO:42001," + x + "," + y;
        CoordinateReferenceSystem auto;
        try {
            auto = CRS.decode(code);
            toTransform = CRS.findMathTransform(
                    DefaultGeographicCRS.WGS84, auto);
            fromTransform = CRS.findMathTransform(auto,
                    DefaultGeographicCRS.WGS84);
            ppolygon = JTS.transform(polygon, toTransform);
        } catch (MismatchedDimensionException | TransformException
                | FactoryException e) {
            logger.error(e.getMessage());
        }

        // buffer
        Geometry out = ppolygon.buffer(distance);
        Geometry retGeom = out;
        try {
            retGeom = JTS.transform(out, fromTransform);
        } catch (MismatchedDimensionException | TransformException e) {
            logger.error(e.getMessage());
        }
        return retGeom;
    }

    private double[][] generatePolygone(List<Point> points){
        double[][] generateSamplePolygon = new double[points.size()][2];
        int i = 0;
        for (Point point : points) {
            generateSamplePolygon[i][0] = point.getX();
            generateSamplePolygon[i][1] = point.getY();
            i++;
        }
        return generateSamplePolygon;
    }

    public double calculateAreaModule(List<Point> points, ToitureEntity toitureEntity){
        TypeToiture typeToiture = defineTypeToiture(new ToitureModification(), toitureEntity);
        logger.debug("type toiture {}",typeToiture.name());
        Double inclinaisonToit  = defineInclinaisonToit(new ToitureModification(), toitureEntity, typeToiture);
        logger.debug("inclinaisonToit {}",inclinaisonToit);

        CardinalPoint cardinalPoint = determineBestCardinalPoint(toitureEntity.getAzimuth());

        if(TypeToiture.Terrasse.equals(typeToiture)){
            double area = GeoGeometry.area(generatePolygone(points));
            area = area / Math.cos(Math.atan((inclinaisonToit/100)));
            return roundTwoDecimal(area);
        }else{
            //Suppression des doublons de points
            List<Point> listWithoutDuplicates = new ArrayList<>(new HashSet<>(points));
            List<Point> listByLongitudeInc = orderListByLongitudeInc(listWithoutDuplicates);
            Point upperLeftPoint = getUpperLeftPoint(listByLongitudeInc);
            Point upperRightPoint = getUpperRightPoint(listByLongitudeInc);
            Point lowerLeftPoint = getLowerLeftPoint(listByLongitudeInc);
            double orthodromicDistanceLargeurRef;
            double orthodromicDistanceLongueurRef;

            if(CardinalPoint.NORTH.equals(cardinalPoint) || CardinalPoint.SOUTH.equals(cardinalPoint)){
                orthodromicDistanceLargeurRef = calculateDistance(upperLeftPoint,lowerLeftPoint);
                orthodromicDistanceLongueurRef = calculateDistance(upperLeftPoint,upperRightPoint);
            }else{
                orthodromicDistanceLargeurRef = calculateDistance(upperLeftPoint,upperRightPoint);
                orthodromicDistanceLongueurRef = calculateDistance(upperLeftPoint,lowerLeftPoint);
            }

            double pente = (orthodromicDistanceLargeurRef / ( 1 * Math.cos(Math.atan((inclinaisonToit/100)))));
            return roundTwoDecimal(pente * orthodromicDistanceLongueurRef);
        }
    }

    private double defautValueInclinaisonToit(TypeToiture typeToiture){
        switch (typeToiture){
            case Terrasse:
                return 0.;
            case Monopente:
                return 7.;
            default:
                return 0.;
        }
    }

    public double calculateAreaRoof(List<Point> points, ToitureModification toitureModification, ToitureEntity toitureEntity){
        TypeToiture typeToiture = defineTypeToiture(toitureModification, toitureEntity);
        logger.debug("type toiture {}",typeToiture.name());
        Double inclinaisonToit  = defineInclinaisonToit(toitureModification, toitureEntity, typeToiture);
        logger.debug("inclinaisonToit {}",inclinaisonToit);

        double azimuth = 180.;
        if(toitureModification.getAzimuth() != null){
            azimuth = toitureModification.getAzimuth();
        }else if(toitureEntity.getAzimuth() != null) {
            azimuth = toitureEntity.getAzimuth();
        }
        CardinalPoint cardinalPoint = determineBestCardinalPoint(azimuth);
        double area;
        if(TypeToiture.Terrasse.equals(typeToiture)){
            area = GeoGeometry.area(generatePolygone(points));
            area = area / Math.cos(Math.atan((inclinaisonToit/100)));
            area = roundTwoDecimal(area);
        }else{
            //Suppression des doublons de points
            List<Point> listWithoutDuplicates = new ArrayList<>(new HashSet<>(points));
            List<Point> listByLongitudeInc = orderListByLongitudeInc(listWithoutDuplicates);
            Point upperLeftPoint = getUpperLeftPoint(listByLongitudeInc);
            Point upperRightPoint = getUpperRightPoint(listByLongitudeInc);
            Point lowerLeftPoint = getLowerLeftPoint(listByLongitudeInc);
            double orthodromicDistanceLargeurRef;
            double orthodromicDistanceLongueurRef;

            if(CardinalPoint.NORTH.equals(cardinalPoint) || CardinalPoint.SOUTH.equals(cardinalPoint)){
                orthodromicDistanceLargeurRef = calculateDistance(upperLeftPoint,lowerLeftPoint);
                orthodromicDistanceLongueurRef = calculateDistance(upperLeftPoint,upperRightPoint);
            }else{
                orthodromicDistanceLargeurRef = calculateDistance(upperLeftPoint,upperRightPoint);
                orthodromicDistanceLongueurRef = calculateDistance(upperLeftPoint,lowerLeftPoint);
            }
            int nombrePan = 1;
            if(TypeToiture.DeuxPans.equals(typeToiture)){
                nombrePan = 2;
            }
            double pente = (orthodromicDistanceLargeurRef / ( nombrePan * Math.cos(Math.atan((inclinaisonToit/100)))));
            area = roundTwoDecimal(pente * orthodromicDistanceLongueurRef);

        }
        logger.debug("surface toiture {}",area);
        return area;
    }

    private TypeToiture defineTypeToiture(ToitureModification toitureModification, ToitureEntity toitureEntity){
        HashMap<String, Object> hashMapToitureModification =  toitureModification.getCaracteristiques();
        HashMap<String, Object> hashMapToitureEntity = toitureEntity.getCaracteristiques();

        if(hashMapToitureModification != null && !hashMapToitureModification.isEmpty() && hashMapToitureModification.containsKey(TYPE)){
            return parseStringToTypeToiture(hashMapToitureModification.get(TYPE).toString());
        }
        if(hashMapToitureEntity != null && !hashMapToitureEntity.isEmpty()){
            if(hashMapToitureEntity.containsKey(TYPE)){
                return parseStringToTypeToiture(hashMapToitureEntity.get(TYPE).toString());
            }else{
                throw new OdsErrorMessages(OdsErrorMessages.CHAMP_MANQUANT_CARACTERISTIQUE_TOITURE,TYPE);
            }
        }
        throw new OdsErrorMessages(OdsErrorMessages.CARACTERISTIQUE_TOITURE_INEXISTANT,toitureEntity.getId());
    }

    private Double defineInclinaisonToit( ToitureModification toitureModification, ToitureEntity toitureEntity, TypeToiture typeToiture){
        HashMap<String, Object> hashMapToitureModification =  toitureModification.getCaracteristiques();
        HashMap<String, Object> hashMapToitureEntity = toitureEntity.getCaracteristiques();

        if(hashMapToitureModification != null && !hashMapToitureModification.isEmpty() && hashMapToitureModification.containsKey(INCLINAISON_TOIT)){
            return Double.valueOf(hashMapToitureModification.get(INCLINAISON_TOIT).toString());
        }
        if(hashMapToitureEntity != null && !hashMapToitureEntity.isEmpty() && hashMapToitureEntity.containsKey(INCLINAISON_TOIT)){
            return Double.valueOf(hashMapToitureEntity.get(INCLINAISON_TOIT).toString());
        }
        return defautValueInclinaisonToit(typeToiture);
    }

    public TypeToiture parseStringToTypeToiture(String typeToitureToParse){
        if(TypeToiture.Terrasse.name().equals(typeToitureToParse)){
            return TypeToiture.Terrasse;
        }else if(TypeToiture.Monopente.name().equals(typeToitureToParse)){
            return TypeToiture.Monopente;
        }else if(TypeToiture.DeuxPans.name().equals(typeToitureToParse)){
            return TypeToiture.DeuxPans;
        }else{
            throw new OdsErrorMessages(OdsErrorMessages.TYPE_TOITURE_INEXISTANT,typeToitureToParse);
        }
    }

    private List<Point> transformPolygonToEPSG4326InEPSG3857Coord(List<Point> points) throws TransformException, FactoryException {
        List<Coordinate> coordinates = points.stream()
                .map(CalculService::pointToCoordinates)
                .collect(Collectors.toList());
        Coordinate[] coordinates1 = coordinates.toArray(new Coordinate[coordinates.size()]);
        CoordinateReferenceSystem epsg4326 = CRS.decode(EPSG_4326,true);
        CoordinateReferenceSystem epsg3857 = CRS.decode(EPSG_3857,true);

        MathTransform mathTransform = CRS.findMathTransform(epsg4326, epsg3857, true);

        List<Point> newPoints = new ArrayList<>();
        for(Coordinate coord : coordinates1){
            Coordinate coordDest = JTS.transform(coord,null,mathTransform);
            newPoints.add(new Point(coordDest.x,coordDest.y));
        }
        return newPoints;
    }

    private List<Point> transformPolygonToEPSG3857InEPSG4326Coord(List<Point> points) throws TransformException, FactoryException {
        List<Coordinate> coordinates = points.stream()
                .map(CalculService::pointToCoordinates)
                .collect(Collectors.toList());
        Coordinate[] coordinates1 = coordinates.toArray(new Coordinate[coordinates.size()]);
        CoordinateReferenceSystem epsg4326 = CRS.decode(EPSG_4326,true);
        CoordinateReferenceSystem epsg3857 = CRS.decode(EPSG_3857,true);
        MathTransform mathTransform = CRS.findMathTransform(epsg3857, epsg4326, true);

        List<Point> newPoints = new ArrayList<>();
        for(Coordinate coord : coordinates1){
            Coordinate coordDest = JTS.transform(coord,null,mathTransform);
            newPoints.add(new Point(coordDest.x,coordDest.y));
        }

        return newPoints;
    }

    private static Coordinate pointToCoordinates(Point point){
        return new Coordinate(point.getX(), point.getY());
    }

    private boolean polygoneOverlapObstacle(double[][] generatePolygone, List<fr.ep.iziglooms.serviceods.common.model.dto.Polygon> obstacles) {
        if(obstacles == null || obstacles.isEmpty()){ return false;}

        for(fr.ep.iziglooms.serviceods.common.model.dto.Polygon obstacle : obstacles){
            double[][] polygoneObstacle = generatePolygone(obstacle);
            boolean overlap = GeoGeometry.overlap(polygoneObstacle, generatePolygone);
            if(overlap){
                return true;
            }
        }
        return false;
    }


    private double[][] generatePolygone(fr.ep.iziglooms.serviceods.common.model.dto.Polygon polygone){
        double[][] generateSamplePolygon = new double[polygone.getCoordinates().get(0).size()][2];
        List<Point> points =  polygone.getCoordinates().get(0).stream()
                .map(doubles -> new Point(doubles.get(0), doubles.get(1)))
                .collect(Collectors.toList());
        int i = 0;
        for (Point point : points) {
            generateSamplePolygon[i][0] = point.getX();
            generateSamplePolygon[i][1] = point.getY();
            i++;
        }
        return generateSamplePolygon;
    }

    private fr.ep.iziglooms.serviceods.common.model.dto.Polygon generatePolygon(List<Point> points){
        fr.ep.iziglooms.serviceods.common.model.dto.Polygon polygon = new fr.ep.iziglooms.serviceods.common.model.dto.Polygon();
        polygon.setType("Polygon");
        List<List<Double>> collect = points.stream()
                .map(point -> {
                    List<Double> coord = new ArrayList<>();
                    coord.add(point.getX());
                    coord.add(point.getY());
                    return coord;
                })
                .collect(Collectors.toList());
        List<List<List<Double>>> coordinates = new ArrayList<>();
        coordinates.add(collect);
        polygon.setCoordinates(coordinates);
        return polygon;
    }

    private fr.ep.iziglooms.serviceods.common.model.dto.Polygon generateModuleGeoTool(Point pointdepart, double longueur, double largeur, double angle, double espacementColonne){
        // 1 étant le point de départ
        //             longueur
        //          1-----------4
        //          |           |
        // largeur  |           |
        //          |           |
        //          2-----------3

        Point point1 = rotatePointGeoTool(pointdepart, angle , espacementColonne);
        Point point2 = rotatePointGeoTool(point1,angle +90, largeur);
        Point point4 = rotatePointGeoTool(point1, angle, longueur);
        Point point3 = rotatePointGeoTool(point4, angle + 90, largeur);
        pointOrigine = point4;

        List<Point> points =  Lists.newArrayList(point1, point2, point3, pointOrigine);

        return generatePolygon(points);
    }

    private Point rotatePointGeoTool(Point center, double angle, double distance){
        try {
            CoordinateReferenceSystem epsg4326 = CRS.decode(EPSG_4326,true);
            GeodeticCalculator calcPoint = new GeodeticCalculator(epsg4326);
            calcPoint.setStartingGeographicPoint(new Point2D.Double(center.getX(),center.getY()));
            calcPoint.setDirection( angle , distance);
            Point2D destPoint1 = calcPoint.getDestinationGeographicPoint();
            return new Point(destPoint1.getX(), destPoint1.getY());
        } catch (FactoryException e) {
            throw new OdsErrorMessages(OdsErrorMessages.ROTATION_MODULE_ERREUR, e.getMessage());
        }
    }

    private Point getUpperLeftPoint(List<Point> points){
        Point point = points.get(0);
        Point point1 = points.get(1);

        if(point.getY() <= point1.getY()){
            return point1;
        }else{
            return point;
        }
    }

    private Point getUpperRightPoint(List<Point> points){
        Point point = points.get(points.size() - 1);
        Point point1 = points.get(points.size() - 2);

        if(point.getY() <= point1.getY()){
            return point1;
        }else{
            return point;
        }
    }

    private Point getLowerRightPoint(List<Point> points){
        Point point = points.get(points.size() - 1);
        Point point1 = points.get(points.size() - 2);

        if(point.getY() <= point1.getY()){
            return point;
        }else{
            return point1;
        }
    }

    private Point getLowerLeftPoint(List<Point> points){
        Point point = points.get(0);
        Point point1 = points.get(1);

        if(point.getY() <= point1.getY()){
            return point;
        }else{
            return point1;
        }
    }

    private List<Point> orderListByLongitudeInc(List<Point> points){
        return points.stream()
                .sorted(Comparator.comparing(Point::getX).thenComparing(Point::getY))
                .collect(Collectors.toList());
    }

    private List<Point> generateListPoints (List<List<List<Double>>> coordinates){
        return coordinates.get(0).stream()
                .map(doubles -> new Point(doubles.get(0), doubles.get(1)))
                .collect(Collectors.toList());
    }

    private Geometry createPolygon(List<Point> points){
        List<Coordinate> coord = points.stream()
                .map(CalculService::pointToCoordinates)
                .collect(Collectors.toList());
        Coordinate[] coordinates = coord.toArray(new Coordinate[coord.size()]);
        GeometryFactory geometryFactory = new GeometryFactory();
        Polygon polygon = geometryFactory.createPolygon(coordinates);
        return geometryFactory.createGeometry(polygon);
    }

    private double roundTwoDecimal(double valueToRound){
        double newValue = valueToRound * 100;
        newValue = Math.round(newValue);
        return newValue / 100;
    }
}
