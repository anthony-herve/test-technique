package fr.ep.iziglooms.serviceods.calculs.model;

import fr.ep.iziglooms.serviceods.common.model.dto.Polygon;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CalculModuleOutput {
    private int nombreColonnes;
    private int nombreRangees;
    private int nombreModules;
    private List<Polygon> modules;
}
