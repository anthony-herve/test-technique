package fr.ep.iziglooms.serviceods.calculs.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.geo.Point;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class MinimumOrientedRectangle {
    private List<Point> points;
    private Long azimuth;
}
