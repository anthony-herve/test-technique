package fr.ep.iziglooms.serviceods.calculs.model;

public enum CardinalPoint {
    EAST,
    SOUTH,
    WEST,
    NORTH;
}
