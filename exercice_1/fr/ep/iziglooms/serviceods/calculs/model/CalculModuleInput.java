package fr.ep.iziglooms.serviceods.calculs.model;

import fr.ep.iziglooms.serviceods.common.model.dto.Polygon;
import fr.ep.iziglooms.serviceods.quantitatif.model.TypeFixation;
import fr.ep.iziglooms.serviceods.toiture.model.dto.TypeToiture;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CalculModuleInput {
    private Polygon champModule;
    private List<Polygon> obstacles;
    private double largeurModule;
    private double longueurModule;
    private double espacementRangees;
    private double espacementColonnes;
    private double distanceAuBord;
    private TypeFixation typeFixation;
    private double inclinaisonToit;
    private TypeToiture typeToiture;
    private Double azimuth;
}
