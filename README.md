# Test technique BACK-END

## Exercice 1
Cet exercice contient un bout de l'application ODS, un outil de dimenssionnement de kits de fixations de panneaux solaires.
La méthode *fr.ep.iziglooms.serviceods.calculs.services.CalculService.calculerModulesGeotools()* permet de déterminer le nombre de modules pouvant être insérés dans un polygone fourni en entrée.
1. En déterminant le meilleur azimuth (ici le sud à 180)
2. En déterminant le *minimum oriented rectangle* du polygone

Après une lecture rapide de la classe *fr.ep.iziglooms.serviceods.calculs.services.CalculService*, propose des axes d'améliorations.
Cela peut être des améliorations sur la lisibilité, les performances, la maintenabilité, etc...


## Exercice 2
Cet exercice est un exercice d'architecture logicielle.

Nous avons une application, **la Bourse aux chantiers**, qui permet à des artisans d'acheter des leads travaux, i.e des chantiers chez les particuliers.

D'un autre côté, nous avons l'application **izigloo** qui permet à des particuliers de répondre à des questionnaires pour décrire leur chantier, et le publier dans la bourse aux chantiers.

Enfin, nous avons une **brique de facturation** qui doit garder l'historique de tous les achats pour permettre une facturation mensuelle.

Le particulier souhaite trouver le meilleur professionnel pour son chantier, le professionnel souhaite trouver des chantiers près de chez lui.

Quelques précisions : 
* Un domaine de compétence est associé à un chantier décrit par le particulier (par exemple plomberie, menuiserie, etc...) pour faire le lien avec les bons artisans.
* Chaque artisan décrit un domaine de compétence et une zone de chalandise (un ensemble de départements), pour lui permettre de trouver les chantiers le concernant.
* La publication d'un chantier déclenche l'envoi d'une notification auprès des artisans concernés (domaine de compétence + zone de chalandise).
* La facturation n'a pas de contrainte temps réel, elle s'effectue à la même date tous les mois. Nous ne nous intéressons pas ici à la partie paiment qui se fait pour l'instant "à l'ancienne".

#### Objectif :
A partir des informations décrites ci-dessus, propose une architecture qui permette de résoudre ce problème.
Pas besoin de nous envoyer une réponse écrite, nous en discuterons de vive voix lors de l'entretien, sauf si bien sûr tu en as envie.